#include <iostream>
#include <cstdio>
#include <string>

void gotoxy(int a, int b){
    std::cout << "\033[" << (b) << ";" << (a) << "f";
}

char getch(){
    system("stty raw");
    char c = getchar();
    system("stty cooked");
    return c;
}


auto main(int argc, char *argv[]) -> int
{
    char c;
    int x = 0;
    int y = 0;
    int width;
    int height;

    std::string green = "\033[0;32m";
    std::string normal = "\033[0;39m";

    std::cout << "\033[2J\033[1;1H";

    if(argc<3){
        width = 10;
        height = 10;
    }
    else {
        if(std::stoi(argv[1]) < 2 || std::stoi(argv[2]) < 2){
            width = 10;
            height = 10;
        }
        else {
            width = std::stoi(argv[1]);
            height = std::stoi(argv[2]);
        }
    }


    while (true)
    {
        // Początkowe ustawienie planszy
        gotoxy(0, 0);
        for(int i = 0; i < width+2; i++){
            std::cout<<"#";
        }
        std::cout << "\n";
        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
                if(j==0) {
                    std::cout << "#";
                }
                if(i==y && x==j){

                    std::cout << green <<"*";
                    std::cout << normal;
                }
                else{
                    std::cout << " ";
                }
                if(j == width-1){
                    std::cout << "#";
                }
            }
            std::cout << "\n";
        }
        for(int i = 0; i < width+2; i++){
            std::cout<<"#";
        }
        std::cout << "\n";
        // Pobranie przycisku i ustawienie *
        c = getch();
        switch(c)
        {
            case 'w':
                y = y - 1;
                if(y<0)
                    y=0;
                break;
            case 'a':
                x = x - 1;
                if(x<0)
                    x=0;
                break;
            case 's':
                y = y + 1;
                if(y == height)
                    y = height-1;
                break;
            case 'd':
                x = x + 1;
                if(x == width)
                    x = width - 1;
                break;
            case 'q':
                std::cout << "uitted\n";
                return 0;
            default:
                break;
        }
        // Reset mapy
        gotoxy(0,0);
        for(int i = 0; i < width+2; i++){
            std::cout<<"#";
        }
        std::cout << "\n";
        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
                if(j==0) {
                    std::cout << "#";
                }
                else{
                    std::cout << " ";
                }
                if(j == width-1){
                    std::cout << "#";
                }
            }
            std::cout << "\n";
        }
        for(int i = 0; i < width+2; i++){
            std::cout<<"#";
        }
        std::cout << "\n";



    }

}




#include <iostream>
#include <string>

auto main(int argc, char* argv[]) -> int
{
    auto const a = std::stoi(argv[1]);
    auto const b = std::stoi(argv[2]);
    
    if(b != 0)
	{
		float answer = float(a) / float(b);
	   	std::cout << answer << "\n";
	}
    else
	{
		std::cout << "Nie można dzielic przez 0" << "\n";
	}

    return 0;
}

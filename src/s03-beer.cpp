#include <iostream>
#include <string>

auto isDigit(char *str) -> bool
{
	while(*str)
	{
		if(!isdigit(*str++)) 
		{ 
			return false; 
		}	
	}
return true;

}

auto fullSong() -> int
{
	auto numberOfBottles = 99;
	auto j = 0;

	for(int i = numberOfBottles; i > 2; --i)
	{
		j = i-1;	
		std::cout << i << " bottles of beer on the wall, "<< i  <<" bottles of beer. Take one down, pass it around, "<< j << " bottles of beer on the wall...\n";

	}
	std::cout << "2 bottles of beer on the wall, 2 bottles of beer. Take one down, pass it around, 1 bottle of beer on the wall...\n";
	std::cout << "1 bottle of beer on the wall, 1 bottle of beer. Take one down, pass it around, no more bottles of  beer on the wall...\n";
	std::cout << "No more bottles of beer on the  wall, no more bottles of beer. Go to store and buy some more, 1 bottle of beer on the wall\n";
return 0;
}


auto main(int argc, char* argv[]) -> int
{
	if(argc > 2) {
        	std::cout << "Za dużo tych piw!\n";
        	return 1;
    	}
	
	else if(argc==1)
	{
		fullSong();
		return 2;
	}

	else if(argc == 2)
	{
		if(!isDigit(argv[1])) 
		{
			std::cout << "To nie jest liczba butelek lub jest ona ujemna!\n";
			return 3;
		}
		auto const numberOfBottles = stoi(std::string(argv[1]));
		auto j = 0;

		if(numberOfBottles > 1)
		{
			for(int i = numberOfBottles; i > 2; --i)
			{
				j = i-1;	
				std::cout << i << " bottles of beer on the wall, "<< i  <<" bottles of beer. Take one down, pass it around, "<< j << " bottles of beer on the wall...\n";

			}
			std::cout << "2 bottles of beer on the wall, 2 bottles of beer. Take one down, pass it around, 1 bottle of beer on the wall...\n";
			std::cout << "1 bottle of beer on the wall, 1 bottle of beer. Take one down, pass it around, no more bottles of  beer on the wall...\n";
			std::cout << "No more bottles of beer on the  wall, no more bottles of beer. Go to store and buy some more, 1 bottle of beer on the wall\n";
		}
		else if (numberOfBottles == 1)
		{	
			std::cout << "1 bottle of beer on the wall, 1 bottle of beer. Take one down, pass it around, no more bottles of  beer on the wall...\n";
			std::cout << "No more bottles of beer on the  wall, no more bottles of beer. Go to store and buy some more, 1 bottle of beer on the wall\n";
		}
		else
		{
			std::cout << "No more bottles of beer on the  wall, no more bottles of beer. Go to store and buy some more, " << numberOfBottles <<" bottles of beer on the wall\n";
		}

	}

	
	
return 0;
}

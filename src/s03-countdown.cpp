#include <iostream>
#include <string>

auto main(int argc, char* argv[]) -> int 
{
    auto const s = std::stoi(argv[1]);
    

    if (s > 0)
    {
        for(auto i = s; i >= 0; i--) {
            std::cout << i << "...\n";
        }
    }
    else if(s < 0)
    {
        for(auto i = s; i <= 0; i++) {
            std::cout << i << "...\n";
        }
    }
    else
    {
        std::cout << "Nie moge odliczac od 0. \n";    
    }

    return 0;
}   
        

#include <iostream>
#include <string>


auto main(int argc, char* argv[]) -> int
{
	// Jeżeli nic nie poda
	if (argc <= 2)
	{
		std::cout << "Cannot run a program.\n";
		return 0;
	}
	// Jeżeli -n
	if (std::string(argv[1]) == "-n")
    	{
        	for(auto i = 1; i < argc-1; i++)
        	{
        		std::cout << argv[i] << " ";
        	}
        	std::cout << argv[argc-1];
    	}

	// Jeżeli -r
    	else if((std::string(argv[2]) != "-l") && (std::string(argv[2]) != "-n") && (std::string(argv[1]) == "-r"))
    	{	  
		for (int i = argc; i > 1; i--)
		{
			std::cout << argv[i-1] << " ";
		}
		std::cout << std::endl;
		return 0;
        
    	}
	// Jeżeli -l
	else if (std::string(argv[1]) == "-l")
	{
        	for(auto i = 1; i < argc; i++)
        	{
        		std::cout << argv[i] << "\n";
        	}	
		return 0;	
	}
    	// Jeżeli -r i -n
	else if((std::string(argv[1]) == "-r") && (std::string(argv[2]) == "-n"))
	{

		for (int i = argc; i > 1; i--)
		{
			std::cout << argv[i-1] << " ";
		}
		return 0;
	
	}
	// Jeżeli -r i -l
	else if((std::string(argv[1]) == "-r") && (std::string(argv[2]) == "-l"))
	{

		for (int i = argc; i > 1; i--)
		{
			std::cout << argv[i-1] << "\n";
		}
		return 0;
	
	}


}

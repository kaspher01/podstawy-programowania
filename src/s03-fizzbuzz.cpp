#include <iostream>
#include <string>

auto isDigit(char *str) -> bool
{
	while(*str)
		if(!isdigit(*str++))
			return false;
		return true;	
}

auto main(int argc, char *argv[]) -> int
{	
	if(argc <= 1)
	{
		std::cout << "Brak argumentów.\n";
		return 1;
	}
	else if(argc > 2) {
        	std::cout << "Za dużo argumentów!\n";
        	return 2;
    	}
	
	if(!isDigit(argv[1])) 
	{
		std::cout << "To nie jest cyfra lub jest ona ujemna!\n";
		return 3;
	}
	else if (std::string(argv[1]) == "0")
	{
		std::cout << "0\n";
		return 3;
	}


	auto const number = stoi(std::string(argv[1]));
	for(auto i = 1; i <= number; i++)
	{
		std::cout << i;
		
		if((i % 3 == 0) && (i % 5 == 0))
		{
			std::cout << " FizzBuzz" << "\n";
		}
		else if (i % 5 == 0)
		{
			std::cout << " Buzz" << "\n";
		}
		else if (i % 3 == 0)
		{
			std::cout << " Fizz" << "\n";
		}
		else
		{
			std::cout << "" << "\n";
		}
	
	}
	
return 0;
}

#include <iostream>

auto main() -> int {
	srand(time(NULL)); // zeby nie losowalo ciagle tej samej liczby
	auto const number = rand() % 100 + 1;
	int your_guess;
	
	do {
		std::cout << "Guess: ";
		std::cin >> your_guess;
		if (number == your_guess)
		{
			std::cout << "Just right!\n";
		}
		else if(number > your_guess)
		{
			std::cout << "Number is bigger\n";
		}
		else if(number < your_guess)
		{
			std::cout << "Number is smaller\n";
		}
	}
	while(number != your_guess);

	return 0;		

}

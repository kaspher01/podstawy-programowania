#include <iostream>
#include <string>

auto main(int argc, char* argv[]) -> int
{
    auto const good = std::string(argv[1]);
    std::string password;

    do {
        std::cout <<"Podaj haslo: "; 
	    std::cin >> password;   
    }
    while(password != good);

    std::cout << "Ok! \n";

    return 0;
}

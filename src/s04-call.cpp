#include <iostream>
#include <cstdlib>
#include <ctime>
auto call_with_random_int(void (*fp)(int const)) -> void {
    srand(time(nullptr));
    int randomInt = rand();
    (*fp)(randomInt);
    std::cout << fp << "\n";
}

void call(int i) {
    std::cout << i;
    std::cout << " Call 1 \n";
}



#include <iostream>
#include <cmath>

struct Rectangle {
    float x;
    float y;
    float scale_x;
    float scale_y;

    auto area() const -> float {
        return (x*y);
    }

    auto draw() const -> void {
        float rx = floor(x * scale_x);
        float ry = floor(y * scale_y);
        for (int i = 1; i <= ry; i++) {
            for (int j = 1; j <= rx; j++) {
                if (i == 1 || i == ry || j == 1 || j == rx)
                    std::cout << "*";
                else
                    std::cout << " ";
            }
            std::cout << "\n";
        }
    }
};

auto main(int argc, char* argv[]) -> int
{
    if(argc != 5){
        return -1;
    }

    struct Rectangle r;
    r.x = atof(argv[1]);
    r.y = atof(argv[2]);
    r.scale_x = atof(argv[3]);
    r.scale_y = atof(argv[4]);

    if(r.x * r.scale_x < 1 || r.y * r.scale_y < 1) {
        return -1;
    }

    r.draw();

    return 0;
}



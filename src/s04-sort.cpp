auto fpsort(void* a[], int n, int (*fp)(void*, void*)) -> void {

    for(int i = n - 1; i >= 0; i--){
        for(int j = 1; j <= i; j++){
            if((*fp)(a[j-1], a[j])>0)
            {
                void* temp_A = a[j-1];
                a[j-1] = a[j];
                a[j] = temp_A;
            }
        }
    }
}

int intcompare(void* first, void* second){
    if(*((int *)first) > *((int *)second))
        return 1;
    else
        return -1;
}

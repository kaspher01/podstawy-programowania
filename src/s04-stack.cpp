#include <iostream>
#include <stack>
#include <cstring>


auto main(int argc, char* argv[]) -> int
{
    if(argc == 1){
        return -1;
    }
    std::stack <char> stringStack;

    // String split
    std::string s = argv[1];
    char arr[s.length()];
    strcpy(arr,s.c_str());

    // Pushing values into stack
    for(int i = 0; i < s.length(); i++) {
        if(arr[i]=='(' || arr[i]=='{' || arr[i]=='[' )
            stringStack.push(arr[i]);
        else if (arr[i]==')' || arr[i]=='}' || arr[i]==']' ){
            if(stringStack.empty()){
                std::cout << "Error" << "\n";
                return -1;
            }
            char s_last = stringStack.top();
            stringStack.pop();
            if((arr[i]==')' && s_last!='(') || (arr[i]==']' && s_last!='[') || (arr[i]=='}' && s_last!='{')) {
                std::cout << "Error" << "\n";
                return -1;
            }
        }
    }
    if (!stringStack.empty()){
        std::cout << "Error" << "\n";
        return -1;
    }

    std::cout << "OK" << "\n";

    return 0;
}

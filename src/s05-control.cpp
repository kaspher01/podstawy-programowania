
#include <iostream>
#include <cstdio>


auto main() -> int
{
    char c;
    int x = 0;
    int y = 0;
    while (true)
    {
        std::cout << "\033[2J\033[1;1H";


        for (int j = 0; j <= y; j++) {
            std::cout << "\n";
            for (int i = 0; i <= x; i++) {
                std::cout << " ";
            }
        }


        std::cout << "*" << "\n";
        std::cout << "\n";
        system("stty raw");
        c = getchar();
        system("stty cooked");

        switch(c)
        {
            case 'w':
                y = y - 1;
                if(y<0)
                    y=0;
                break;
            case 'a':
                x = x - 1;
                if(x<0)
                    x=0;
                break;
            case 's':
                y = y + 1;
                break;
            case 'd':
                x = x + 1;
                break;
            case 'q':
                std::cout << "uitted\n";
                return 0;
            default:
                break;
        }


    }

}


